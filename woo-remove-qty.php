<?
/*
Plugin Name: Woocommerce Remove Quantity
Plugin URI: http://stillarts.com
Description: Remove the Qty from the product page, cart, checkout and one-page-checkout in Woocommerce
Version: 0.3
Author: Marirs, MrSwed
Author URI: http://stillarts.com
GitLab URI: https://gitlab.com/sdwp/plugins/woocommerce-remove-quantity-fields
License: GPL2


    Copyright 2012  WooCommerce Remove Quantity  (email : marirs@aol.in)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

define( 'WOO_REMOVE_QTY_VERSION', '0.3' );

function callback( $buffer ) {

	/* Remove the Div Class from the product page */
	$contents = $buffer;
	$pattern  = '#\<div class="quantity"\>\s*(.+?)\s*\<\/div\>#s';
	$contents = preg_replace_callback(
		$pattern,
		function ( $matches ) { return "<!--<div class=quantity>$matches[1]</div>-->"; },
		$contents
	);

	/* Remove the TH from the Cart page */
	$pattern  = '#\<(th|td) class="product-quantity"[^\>]*\>\s*(.+?)\s*\<\/\\1\>#s';
	$contents = preg_replace_callback(
		$pattern,
		function ( $matches ) { return "<!--<$matches[1] class=product-quantity>$matches[2]</$matches[1]>-->"; },
		$contents
	);

	$pattern  = '#\<(td|div) class="product-quantity"[^\>]*\>\s*<!--(.+?)-->\s*<\/\\1\>#s';
	$contents = preg_replace_callback(
		$pattern,
		function ( $matches ) { return "<!--<$matches[1] class=product-quantity>$matches[2]</$matches[1]>-->"; },
		$contents
	);

	/* Remove the div from the one page cart code */
	$pattern  = '#\<(strong|div|span) class="product-quantity"\>\s*([^\<\>]+?)\s*\<\/\\1\>#s';
	$contents = preg_replace_callback(
		$pattern,
		function ( $matches ) { return "<!--<$matches[1] class=product-quantity>$matches[2]</$matches[1]>-->"; },
		$contents
	);

	$buffer = $contents;

	return $buffer;
}

function woo_no_qty_css() { ?>
	<style type="text/css" data-called="woo_no_qty"> 
		.woocommerce-cart-form .product-quantity,
		.woocommerce.checkout .product-quantity
		{ display: none; }
	</style>
	<?php
}

function buffer_start() { ob_start( "callback" ); }

function buffer_end() { ob_end_flush(); }

add_action( 'wp_head', 'buffer_start' );
add_action( 'wp_footer', 'buffer_end' );
add_action( 'wp_head', 'woo_no_qty_css' );
?>